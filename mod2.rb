#Write a program to define a method inside a module and then use the module in such a way that
#the method can be accessible as an instance method of that class.
module ModuleA
  def func
    puts " This is a func from module a"
  end
end

class Test
  include ModuleA
end

t = Test.new
t.func