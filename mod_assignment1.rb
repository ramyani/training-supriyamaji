#Write a program to define two modules and both having a method with same name
#but with different functionality and then define a method in a class and use those module methods inside that.
module ModuleA
  def Module_a.func
    puts " This is a func from module aaa"
  end
end

module ModuleB
  def Module_b.func
    puts " This is a func from module b"
  end
end

class Cls
  include ModuleA
  include ModuleB

  def fun
    ModuleA.func
    ModuleB.func
  end
end

ob = Cls.new
ob.fun

