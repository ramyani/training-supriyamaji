class Assignment4
  #Write a program to generate a hash from a given hash where the value of each key will be multiplied with 2
  def mult(options)
    if options.empty?
      puts "hash is empty"
    else
      options.each do |key, value|
        if value == nil
          options[key] = nil
        else
          options[key] = value * 2
        end
      end
    end
  end

  #Write a program to exchange key and value and generate a new hash
  def exchng(hash1)
    hash2 = Hash.new
    hash1.each do |key, value|
      if value == nil
        hash1.delete(key.to_sym)
      end
    end

    hash2 = hash1.invert
    #hash2 [ value ] = key.to_s
  end

  #Write a program to convert each key value as separate hash and return an array of hashes.eg: Input: {:a => 1, :b => 2, :c => 3}
  #Output: [{:a => 1}, {:b => 2}, {:c => 3}]
  def convert_hash(hash1)
    if hash1.empty?
      puts "hash is empty"
    else
      arr = []

      hash1.each do |key, value|
        h2 = { key => value }
        arr << h2
      end

      arr
    end
  end

  #Write a program to convert  { a: 'e', b: 'm', c: nil, d: 'q' } to  [[:a, "e"], [:b, "m"], [:c, nil], [:d, "q"]]
  def convert_array(hash1)
    if hash1.empty?
      puts "hash is empty"
    else
      arr = []
      hash1.each do |key, value|
        arr << [key, value]
      end

      arr
    end
  end

  #Write a program to add new key value to an existing hash
  def add_key_value(h1, key, value)
    key=key.to_sym
    h1.merge!( key => value)
  end

  #Write a program to delete a specific key from a hash
  def delete_key_value(h1, key)
    h1.delete(key.to_sym)
    h1
  end

  # h1={ a: 12, b: 13, c: nil, d: 10 }
  #ob=Assignment4.new
  #ob.mult(h1)
  #ob.exchng(h1)
  #ob.convert_hash(h1)
  #ob.convert_array(h1)
  #ob.add_key_value(h1,'g',200)
  #ob.delete_key_value(h1,b)
end