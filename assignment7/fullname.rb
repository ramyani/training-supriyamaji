# Create a function which will take that lambda block as an argument and it will take two more arguments first name and last name. Then call that
#lambda such a way so that it will use that lamda block to generate full name from the given arguments.
def full_name_lam(lam,fname,lname)
  lam.call(fname,lname)
end

lam = lambda{ |fname, lname| puts "fullname is #{ fname } " " #{ lname } " } #Define a lambda which will take 2 arguments first name and last name and will generate fullname.
full_name_lam(lam,"aaaaa","bbbbb")

#Do the same thing mentioned above using proc instead of lambda.
def full_name_proc(proc,fname,lname)
  proc.call(fname,lname)
end

proc = Proc.new { | fname,lname | puts "fullname is #{ fname }" " #{ lname } " }
full_name_proc(proc,"sssss","mmmmmmm")

#Write a program to describe two difference between proc and lambda.
def lambda_func
  my_lam = lambda { return }
  my_lam.call
  #my_lam.call(2) #error
  puts"hello from lambda func" #this will print
  p my_lam.class #same Proc class
end

lambda_func

def proc_func
  my_proc = Proc.new { return }
  p my_proc.class #proc class
  my_proc.call
  my_proc.call(2) # no error
  puts"hello from proc" #this will not print

end

proc_func

#Define a method which will take a block which will generate the full name from first name and last name.
def full_name_block
  yield('block', 'func')
end

full_name_block do | a, b |
  puts a +' '+ b
end


