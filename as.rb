def as1(str)
  a=str.to_f
  puts a
end
def as2(str)
  a=str.delete('aeiouy')
  puts a
end
def as3
  a="hello"+" "+"world"
  puts a
end
def as4(str)
  a=str.capitalize()
  puts a
end
def as5(str)
  a=str.upcase
  puts a
end
def as6(str)
  a=str.gsub('Ruby','Rail')
  puts a
end
def as7(str)
  a=str.strip
  puts a
end
def as8(first_name,last_name)
  puts "full name #{first_name} #{last_name}"
end
def as9(str)
  p str.to_sym
end
def as10(str,a,b)
  p=str.slice(a,b)
  puts p
end
as1("12.3")
as2("Learning Ruby")
as3()
as4("The Ruby Book")
as5("The Ruby Book")
as6("The Ruby Book is based on Ruby")
as7(" Ruby on Rails")
fn=as8(nil,"Maji")
as9('ruby')
as10("hellloooo",5,10)



