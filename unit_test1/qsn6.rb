class PrivateProtected

  def private_method_with_self
    private_method
  end
  def private_method_with_self_expl
    self.private_method
  end
  def protected_method_with_self
    self.protected_method
  end
  private
  def private_method
    puts "this is a private method"
  end
  protected
  def protected_method
    puts "this is protected method"
  end
end
ob=PrivateProtected.new
#ob.private_method #error
#ob.private_method_with_self_expl
ob.private_method_with_self #no error
#ob.protected_method #error
ob.protected_method_with_self



