class Base
  # method of superclass accpeting
  # two parameter
  def display (a = 0, b = 0)
    "Parent class, 1st Argument: #{a}, 2nd Argument: #{b}"
  end

  #no parameter
  def display2
    "This is without argument"
  end
end

# derived class Sub_cls
class SubCls < Base
  def display(a, b)
    # passing both the argument
    super(a, b)
    "Hey! This is subclass method with argument"
  end

  def display2
    super
    "Hey! This is subclass method"
  end
end
#obj=Sub_cls.new.display2
#obj=Sub_cls.new.display("bbb","cccc")


