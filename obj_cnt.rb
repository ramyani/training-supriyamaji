class Customer
   @@no_of_customers = 0
   def initialize(id, name, addr)
      @cust_id = id
      @cust_name = name
      @cust_addr = addr
      @@no_of_customers += 1
   end

   def self.total_no_of_customers
      "Total number of customers: #@@no_of_customers"
   end
end

#cust1 = Customer.new("1", "sm", "kol")
#Customer.total_no_of_customers
#cust2 = Customer.new("2", "am", "asn")
#Customer.total_no_of_customers