class Assignment3
  #Write a program to get all the words which are starting from 'ab'
  def strting_with(arr)
    arr.find_all { |val| val.start_with? "ab" }
    rescue
      "enter valid input"
  end

  #Write a program to convert ['abc bcd', 'mno pqr xyz', 'efg'] to ['abc', 'bcd', 'mno', 'pqr', 'xyz', 'efg']
  def convert(arr)
    arrnew = Array[]
    arr.each do |value|
      arrnew = arrnew.concat(value.split)
    end
    p arrnew
    rescue
      "invalid"
  end

  #Write a program to get first element which is non zero number.
  def first_non_zero(arr)
    begin
      arr.find { |val| !val.zero? }
    rescue
      "invalid input"
    end
  end

  #Write a program to check whether all element is non zero? (Method should return either true or false based on given input)
  def is_non_zero(arr)
    begin
      arr.find_all { |val| val.zero? }.empty?
    rescue
      "invalid input"
    end
  end

  #Write a program to check whether any element is an even number? (Method should return either true or false based on given input)
  def is_even(arr)
      arr.any? { |val| val.even? }
    rescue
      "invalid input"
  end
end