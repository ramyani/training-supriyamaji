class Base
  def initialize(a)
    @var1 = a
  end
end

class SubCls < Base
  def initialize(a, b)
    super(a)
    @var2 = b
  end
end

class SubCls2 < Base
  def initialize(a)
    super
  end
end
