  Rails.application.routes.draw do
  resources :microposts
    resources :users
    resources :books
    resources :static_pages
    resources :items
    get 'about' => 'order#about'
    get '/books/show_subjects/:id', to: 'books#show_subjects', as: 'show_subjects'
  end