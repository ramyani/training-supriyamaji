require "faker"
require 'csv'
def write_csv
  array = []
  #array = [ { id: 1, fname: "john", lname:"de" } , {id: 2, fname: "amal", lname: "roy"}, {id: 3, fname: "subhas", lname: "basu" } ]
  h2=Hash.new
  for i in 0..50
        h2 = { "fname"=>Faker::Name.first_name ,"lname"=>Faker::Name.last_name,"id"=>Faker::Number.number(digits: 1)}
        array << h2
 end
  col_name = array.first.keys
  s = CSV.generate do |csv|
    csv << col_name
    array.each do |x|
      csv << x.values
    end
  end
  File.write('the_file.csv', s)
end

def read_csv
  CSV.foreach('the_file.csv') do |row|
  p row
  end
end

def insert_csv
  rows = CSV.read('the_file.csv', headers: true).collect do |row|
    row.to_hash
  end
  column_names = rows.first.keys
  additional_column_names = ['fullname']
  column_names += additional_column_names
  s = CSV.generate do |csv|
    csv << column_names
    rows.each do |row|
      values = row.values
      row_new =  row["fname"] + " "+ row["lname"]
      additional_values_for_row = [row_new]
      values += additional_values_for_row
      csv << values
    end
  end
  File.open('the_file.csv', 'w') { |file| file.write(s) }
end

def sort_csv(per_page,page,srt_by,order)
  fr = per_page*(page-1)+1
  t = per_page*page
  p fr
  p t
  arr = CSV.read('the_file.csv', headers: true).collect do |row|
    row.to_hash
  end

  sarr = sortarr = arr.slice(fr,per_page).sort_by do |item|
    item[srt_by]
  end
  if(order=="dsc")
    sarr=sortarr.reverse
  end
  p sarr
  s = CSV.generate do |csv|
    sarr.each do |x|
      csv << x.values
    end
  end
  File.write('sort.csv', s)

end

