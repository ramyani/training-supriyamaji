#print the pattern
     #
    # #
   # # #
  # # # #
 # # # # #
# # # # # #
def stair(n)
  for i in (1..n)
    puts ' ' * (n-i)+ '# ' * i
    #puts i
  end
end

#print the pattern
#
# #
# # #
# # # #
# # # # #
# # # # # #
# # # # #
# # # #
# # #
# #
#


def stair2(n)
  for i in (1..n)
    puts '# ' * (i)
  end
  for i in (1...n)
    puts '# ' * (n-i)
  end
end

#print the pattern
     #
    ##
   ###
  ####
 #####
######
 #####
  ####
   ###
    ##
     #


def stair3(n)
  for i in 1..n
    puts ' ' * (n-i) + '#' * (i)
  end
  for i in 1...n
    puts ' ' * (i) + '#' * (n-i)
  end

end