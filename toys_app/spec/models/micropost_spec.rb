require 'rails_helper'

RSpec.describe Micropost, type: :model do
  context 'validations' do
    context 'presence of content' do
      context 'if user does not have content' do
        it 'should not be valid micropost' do
          micropost = Micropost.new
          expect(micropost).to_not be_valid
        end
      end
    end
  end

end
