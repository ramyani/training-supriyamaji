require 'rails_helper'

RSpec.describe User, type: :model do
    let(:user) { User.new(name: 'Test User', email: 'test@test.com',password: '123456' ,password_confirmation: '123456') }

  context 'validation' do
    context 'presence of email' do
      context 'if user does not have email ' do
        it 'should not be valid user' do
          user = User.new
          expect(user).to_not be_valid
        end
      end
    end

    context 'if user has not name, email and password' do
        it 'should not be valid user' do
          user = User.new(name: 'Test User', email: 'test@test.com')
          expect(user).to_not be_valid
        end
      end
  end

  context 'if users should reject invalid email'
      it 'email validation should accept valid addresses' do
        valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
        valid_addresses.each do |valid_address|
        user.email = valid_address
        expect(user).to_not be_valid
      end
   end

    it "name should be present" do
      user.name = "     "
      expect(user).to_not be_valid
    end

    it "email should not be too long" do
      user.email = "a" * 244 + "@example.com"
      expect(user).to_not be_valid
    end

    it  "email addresses should be unique" do
      duplicate_user = user.dup
      user.save
      expect(duplicate_user).to_not be_valid
    end
    it "password should be present (nonblank)" do
      user.password = user.password_confirmation = " " * 9
      expect(user).to_not be_valid
    end

    it "password should have a minimum length" do
      user.password = user.password_confirmation = "a" * 7
      expect(user).to_not be_valid
    end

    it "authenticated? should return false for a user with nil digest" do
      expect(user.authenticated?('')).to eq(false)
    end





   # context 'password length should be at least 8 char'
   #     it { should validate_length_of(:password).is_at_least(8).with_message(/password is too short/) }



  # context 'if password does not match'
  #   it 'password mismatch' do
  #      user.password = user.password_confirmation
  #      expect(user).to_not be_valid
  #   end
  # end






  #  describe '#full_name' do
  #   it 'should return proper value' do
  #     expect(user.full_name).to eq('My')
  #   end
  # end
end

