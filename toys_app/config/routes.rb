Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  get 'users/new'
   get 'help'    => 'static_pages#help'
   get 'about'    => 'static_pages#about'
  get 'home'   => 'static_pages#home'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :microposts, only: [:create, :destroy]

  #get 'logout'  => 'sessions#destroy'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users
  resources :microposts
  resources :relationships,       only: [:create, :destroy]
  #resources :sessions
  root 'static_pages#home'
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :users do
    member do
      get :following, :followers
    end
  end

end
