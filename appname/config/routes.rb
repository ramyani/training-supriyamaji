  Rails.application.routes.draw do
    resources :users
    resources :books
    get 'books/show_subjects'
  end