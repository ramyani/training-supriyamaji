class Amount

  attr_accessor :total_amount
  def initialize(price, quantity)
    @price = price
    @quantity = quantity
    self.total_amount = price*quantity
  end
end

ob = Amount.new(200, 4)
ob.total_amount
