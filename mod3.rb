#Write a program to define a method inside a module and then use the module in such a way
#that the method can be accessible as a class method of that class.
module ModuleD
  def func
    puts " This is a func from module a"
  end
end

class Test
  extend ModuleD
end

Test.func
