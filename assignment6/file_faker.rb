# 1. Create an User class with first name, last name and age.

# 2. Create mock users using faker gem and store them in a CSV file.

# 3. Read the csv file and process data and append a full name column which will be generated from first name and last name.

# 4. Impliment pagination and sorting on the users data and store the paginated result in a new csv file.

#    Write a function which will take follwoing arguments as input:

#       sort_by (it should be any valid column name),

#       order ('asc' / 'desc'),

#       per_page (it should be an integer number which refers how many records you want to see),

#       page (it should be an integer number which indicates the page which you want to see)



class User

  require "faker"
  require 'csv'
  attr_accessor :fname
  attr_accessor :lname
  attr_accessor :age
  def initialize(fname, lname, age)
    @fname=fname
    @lname=lname
    @age=age
  end

  def self.mock_user
    user_array=[]
    for i in 0..50
      user_array << User.new(Faker::Name.first_name,Faker::Name.last_name,Faker::Number.number(digits:2))
    end

    col_name=["fname", "lname", "age"]

    s = CSV.generate do |csv|
      csv << col_name
      #user_array[1].fname
      user_array.map do |obj|
        csv << [obj.fname, obj.lname, obj.age]
      end
    end

    File.write('mock.csv', s)

  end

  def self.read_csv
    CSV.foreach('mock.csv') do |row|
      p row
    end
  end

  def self.insert_csv
    rows = CSV.read('the_file.csv', headers: true).collect do |row|
      row.to_hash
    end

    column_names = rows.first.keys
    additional_column_names = ['fullname']
    column_names += additional_column_names
    s = CSV.generate do |csv|
      csv << column_names
      rows.each do |row|
        values = row.values
        row_new =  row["fname"] + " "+ row["lname"]
        additional_values_for_row = [row_new]
        values += additional_values_for_row
        csv << values
      end
    end
    File.open('the_file.csv', 'w') { |file| file.write(s) }
  end

  def self.sort_csv(per_page, page, srt_by, order)
    fr = per_page * (page - 1) + 1
    t = per_page * page
    p fr
    p t
    arr = CSV.read('mock.csv', headers: true).collect do |row|
      row.to_hash
    end

    sortarr = arr.slice(fr, per_page).sort_by do |item|
      item[srt_by]
    end
    if(order == "asc")
      sarr = sortarr
    end
    if(order == "desc")
      sarr = sortarr.reverse
    end
    #p sortarr
    #p sarr
    s = CSV.generate do |csv|
      sarr.each do |x|
        csv << x.values
      end
    end
    File.write('sort.csv', s)
  rescue
    puts "Enter properly. Order must be asc/desc "
  end
end

