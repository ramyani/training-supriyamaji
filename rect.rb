class Rectangle
   def initialize(l,b)
      @len = l.to_i
      @br = b.to_i
   end

   def per
   	   @len + @br
   end

   def area
   	  @len * @br
   end
end
r1 = Rectangle.new(2, 3)
r1.per
r1.area