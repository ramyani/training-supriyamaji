def convert_hash(hash_original)
  if hash_original.empty?
    puts "hash is empty"
  else
    arr = []
    arr.tap do |arr|
      hash_original.each do |key, value|
        hash_new = { key => value }
        arr << hash_new
      end
    end
    arr
  end
end